package com.oss.urlshortenerservice.service;

import com.oss.urlshortenerservice.repository.ShortenUrlRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.atMostOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Service Class Unit Tests.
 */
class ShortenUrlServiceTest {

    @Mock
    ShortenUrlRepository shortenUrlRepository;

    @InjectMocks
    private ShortenUrlServiceImpl shortenUrlService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testConvertingToShortenUrl() {
        String requestUri = "http://localhost:8080/v1/api/create-short-url";
        String requestUrl = "https://www.linkedin.com";
        String expectedUri = "http://localhost:8080/v1/api/8418d412";

        String shortUrl = shortenUrlService.convertUrlToShortenUrl(requestUrl, requestUri);
        assertEquals(expectedUri, shortUrl);
    }

    @Test
    void testRetrievingShortenUrl() {
        String requestUri = "http://localhost:8080/v1/api/8418d412";
        String originalUrl = "https://www.linkedin.com";
        when(shortenUrlService.retrieveUrlFromShortId(requestUri)).thenReturn(originalUrl);
        verify(shortenUrlRepository, atMostOnce()).getOriginalUrl(originalUrl);
        assertEquals(originalUrl, shortenUrlService.retrieveUrlFromShortId(requestUri));
    }
}
