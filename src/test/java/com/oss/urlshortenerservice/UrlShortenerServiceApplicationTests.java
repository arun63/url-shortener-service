package com.oss.urlshortenerservice;

import com.oss.urlshortenerservice.domain.CreateUrlRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 * Integration and Functional tests.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UrlShortenerServiceApplicationTests {

    private static final CreateUrlRequest createUrlRequest = new CreateUrlRequest("https://www.linkedin.com");
    private static final String requestMapper = "/v1/api/";
    private static final String requestOriginURI = "http://127.0.0.1:";
    private static final String shortenUrlId = "8418d412";
    @Autowired
    private TestRestTemplate restTemplate;
    @LocalServerPort
    private int port;

    @Test
    void contextLoads() {
    }

    @Test
    public void testCreateShortenUrlSuccess() throws Exception {
        String expectedShortenUrl = requestOriginURI + port + requestMapper + shortenUrlId;
        String shortenUrl = this.restTemplate.postForObject(requestOriginURI + port + requestMapper + "create-short" +
                "-url", createUrlRequest, String.class);
        assertEquals(expectedShortenUrl, shortenUrl);
    }

    @Test
    public void testCreateShortenUrlFailure() throws Exception {
        CreateUrlRequest createUrlRequest = new CreateUrlRequest("");
        String shortenUrl = this.restTemplate.postForObject(requestOriginURI + port + requestMapper + "create-short" +
                "-url", createUrlRequest, String.class);
        assertEquals(null, shortenUrl);
    }

    @Test
    public void testRetreiveUrlFailure() throws Exception {
        String shortenUrl = requestOriginURI + port + requestMapper + shortenUrlId;
        this.restTemplate.postForObject(requestOriginURI + port + requestMapper + "create-short-url",
                createUrlRequest, String.class);
        String requestUrl = this.restTemplate.getForObject(shortenUrl, String.class);
        if (requestUrl != null) {
            assertFalse(requestUrl.contains("youtube.com"));
        }
    }
}
