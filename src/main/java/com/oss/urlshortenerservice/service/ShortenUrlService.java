package com.oss.urlshortenerservice.service;

/**
 * Interface for the service implementation.
 */
public interface ShortenUrlService {

    /**
     * The method use to convert the url to shorten url.
     * @param requestUrl long formatted string of URL to be shorten
     * @param requestOrigin The request origin.
     * @return a shorten url
     */
    String convertUrlToShortenUrl(String requestUrl, String requestOrigin);

    /**
     * The method to retrieve the original url from its id.
     * @param shortUrlId shorten url id.
     * @return a long formatted string for the given id.
     */
    String retrieveUrlFromShortId(String shortUrlId);
}
