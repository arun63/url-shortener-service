package com.oss.urlshortenerservice.service;

import com.google.common.hash.Hashing;
import com.oss.urlshortenerservice.repository.ShortenUrlRepository;
import com.oss.urlshortenerservice.utils.ShortenUrlUtils;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;

/**
 * Shorten URL Service implementation.
 * Non-Cryptographic Hashing is introduced.
 */
@Service
public class ShortenUrlServiceImpl implements ShortenUrlService {

    ShortenUrlRepository shortenUrlRepository;

    public ShortenUrlServiceImpl(ShortenUrlRepository shortenUrlRepository) {
        this.shortenUrlRepository = shortenUrlRepository;
    }

    @Override
    public String convertUrlToShortenUrl(String longUrl, String requestUrl) {
        // Using third-party client for non-cryptographic hashing such as MurmurHash3
        // The hash function used is 32-bit and to increase the complex and uniqueness
        // we can use murmur3_128 as an enhancement.
        final String shortenId = Hashing.murmur3_32().hashString(longUrl, StandardCharsets.UTF_8).toString();
        shortenUrlRepository.storeShortenUrl(shortenId, longUrl);

        return ShortenUrlUtils.getRequestOriginUri(requestUrl) + shortenId;
    }

    @Override
    public String retrieveUrlFromShortId(String shortenId) {
        return shortenUrlRepository.getOriginalUrl(shortenId);
    }
}
