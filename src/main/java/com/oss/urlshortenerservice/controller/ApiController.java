package com.oss.urlshortenerservice.controller;


import com.oss.urlshortenerservice.domain.CreateUrlRequest;
import com.oss.urlshortenerservice.service.ShortenUrlService;
import org.apache.commons.validator.routines.UrlValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Application Controller.
 * We define two end points POST and GET to set and get the shorten url.
 */
@RestController
@RequestMapping("v1/api")
public class ApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiController.class);

    private final ShortenUrlService shortenUrlService;

    public ApiController(ShortenUrlService shortenUrlService) {
        this.shortenUrlService = shortenUrlService;
    }

    @RequestMapping(value = "/create-short-url", method = RequestMethod.POST, consumes = {"application/json"})
    public ResponseEntity<String> createShortUrl(@RequestBody final CreateUrlRequest createShortUrl,
                                                 HttpServletRequest request) {
        LOGGER.info("Received Url Shorten request - {} for the url: {}", request.getRequestURL().toString(),
                createShortUrl.getOriginalUrl());

        UrlValidator urlValidator = new UrlValidator(new String[]{"http", "https"});

        if (urlValidator.isValid(createShortUrl.getOriginalUrl())) {
            String requestOrigin = request.getRequestURL().toString();
            String shortenUrl = shortenUrlService.convertUrlToShortenUrl(createShortUrl.getOriginalUrl(),
                    requestOrigin);
            LOGGER.info("Shortened Url for the request: {} is {}", createShortUrl.getOriginalUrl(), shortenUrl);
            return new ResponseEntity<>(shortenUrl, HttpStatus.CREATED);
        }

        LOGGER.error("Invalid long url for the request : " + request.getRequestURL());

        /**
         * TODO: Create Response Object for error handling.
         */
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public void retrieveOriginalUrl(@PathVariable String id, HttpServletResponse response) throws Exception {
        LOGGER.info("Retrieving Original URL from the short url id: " + id);

        final String originalUrl = shortenUrlService.retrieveUrlFromShortId(id);
        if (null != originalUrl) {
            response.sendRedirect(originalUrl);
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}
