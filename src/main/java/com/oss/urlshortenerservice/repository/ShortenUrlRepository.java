package com.oss.urlshortenerservice.repository;

/**
 * Interface provider for the shorten url service repository.
 */
public interface ShortenUrlRepository {

    /**
     * Store the shorten url into the repository.
     * @param id the unique identifier for the url.
     * @param originalUrl the original url string.
     */
    void storeShortenUrl(String id, String originalUrl);

    /**
     * Retrieve the original url from its id.
     * @param id the unique identifier.
     * @return the long formatted url string.
     */
    String getOriginalUrl(String id);
}
