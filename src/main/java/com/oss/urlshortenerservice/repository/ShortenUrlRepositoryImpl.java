package com.oss.urlshortenerservice.repository;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

/**
 * The implementation uses the redis database to store the information.
 * Redis Embedded server makes the configuration easier and provide in-memory data store access.
 * This performance better when the url are shorted lived and are stateless in nature.
 */
@Repository
public class ShortenUrlRepositoryImpl implements ShortenUrlRepository {

    RedisTemplate<String, String> redisTemplate;

    public ShortenUrlRepositoryImpl(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public void storeShortenUrl(String shortUrlId, String originalUrl) {
        redisTemplate.opsForValue().set(shortUrlId, originalUrl);
    }

    @Override
    public String getOriginalUrl(String shortUrlId) {
        return redisTemplate.opsForValue().get(shortUrlId);
    }
}
