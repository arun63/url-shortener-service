package com.oss.urlshortenerservice.utils;

/**
 * Utility Class.
 */
public class ShortenUrlUtils {

    /**
     * The method util to format the shorten url.
     *
     * @param requestOrigin the request origin.
     * @return a new string uri.
     */
    public static String getRequestOriginUri(String requestOrigin) {

        String[] originUriPaths = requestOrigin.split("/");
        StringBuffer stringBuffer = new StringBuffer();
        // rebuilding the request origin uri path without the create-short-url at the end.
        for (int i = 0; i < originUriPaths.length - 1; ++i) {
            stringBuffer.append(originUriPaths[i]);
            stringBuffer.append('/');
        }
        return stringBuffer.toString();
    }

}
